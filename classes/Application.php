<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 15:22
 */

class Application {

    /**
     * Holds application instance
     *
     * @var null
     */
    static $instance = null;

    /**
     * App config holder
     *
     * @var null
     */
    protected $_config = null;

    /**
     * Config file name holder
     *
     * @var string
     */
    protected $_configFileName = 'config.xml';

    /**
     * Class constructor
     *
     */
    protected function  __construct() {  }

    /**
     * Returns application singleton
     *
     * @return Application
     */
    public static function instance()
    {
        if (self::$instance instanceof Application) {
            return self::$instance;
        }

        $instance = self::$instance = new self();

        return $instance;
    }

    /**
     * Returns application config
     *
     * @return SimpleXMLElement
     */
    public function getConfig()
    {
        if ($this->_config===null) {
            $simpleXml = simplexml_load_file($this->_configFileName);
            $this->_config = $simpleXml;
        }

        return $this->_config;
    }

    /**
     * Returns app config value
     *
     * @param $path
     * @return SimpleXMLElement
     */
    public function getConfigValue($path, $asString = true)
    {
        $value = $this->getConfig()->xpath($path);

        if (count($value)===0) {
            return false;
        }

        if (count($value)>1) {
            return $value;
        }

        if (count($value)===1 && $asString) {
            return (string) $value[0];
        }

        return $value;
    }

    /**
     * Are we in CLI area?
     *
     * @return bool
     */
    public function CLI()
    {
        return php_sapi_name()==='cli';
    }
}