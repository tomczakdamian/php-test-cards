<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 14:53
 */

class Deck
{
    /**
     * Card colors
     *
     * @var array
     */
    protected $_colors = array();

    /**
     * Class constructor
     *
     * @param array $colors
     */
    public function __construct(array $colors = null)
    {
        if (is_array($colors)) {
            foreach ($colors as $color) {
                $this->addColorCollection($color);
            }
        }
    }

    /**
     * Add collection to the deck
     *
     * @param Card_Collection_Interface $collection
     * @return $this
     */
    public function addColorCollection(Card_Collection_Interface $collection)
    {
        /**
         * We could check in this point whether the specified color is not in the collection already
         * but this depends of the 'game' requirements
         */
        $this->_colors[] = $collection;

        return $this;
    }

    /**
     * Return colors
     *
     * @return array
     */
    public function getColors()
    {
        return $this->_colors;
    }

    /**
     * Shuffle the collection
     *
     * @return bool
     */
    public function shuffle()
    {
        foreach ($this->getColors() as $color) {
            $color->shuffle();
        }

        return true;
    }
}