<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 12:33
 */

class Card_Diamond
    extends Card_Abstract
{
    /**
     * Returns color name
     *
     * @return string
     */
    public function getColorName()
    {
        return 'Diamond';
    }
}