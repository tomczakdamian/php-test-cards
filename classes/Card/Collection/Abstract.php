<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 12:53
 */

abstract class Card_Collection_Abstract
    implements Card_Collection_Interface, Iterator
{
    /**
     * Position holder (for the iterator req.)
     *
     * @var int
     */
    protected $_position = 0;

    /**
     * Cards holder
     *
     * @var array
     */
    protected $_cards = array();

    /**
     * Sort cards in the collection in the valid order
     *
     * @return bool
     */
    public function sortInValidOrder()
    {
        $cards = array();

        foreach ($this->_cards as $card) {
            $cards[$card->getType()] = $card;
        }

        ksort($cards);

        $this->_cards = $cards;

        return true;
    }

    /**
     * We can use this generator instead of the iterator functionality.
     */
    public function getGenerator()
    {
        foreach ($this->_cards as $card) {
            yield $card;
        }
    }

    /**
     * Shuffle cards in the collection
     *
     * @return bool
     */
    public function shuffle()
    {
        shuffle($this->_cards);

        return true;
    }

    /**
     * Returns the cards associated to the current collection
     *
     * @return array
     */
    public function getCards()
    {
        return $this->_cards;
    }

    /**
     * Returns current card (iterator req.)
     *
     * @return Card_Interface
     */
    public function current()
    {
        return $this->_cards[$this->_position];
    }

    /**
     * Returns current position (iterator req.)
     *
     * @return int
     */
    public function key()
    {
        return $this->_position;
    }

    /**
     * Returns current position (iterator req.)
     *
     * @return void
     */
    public function next()
    {
        $this->_position++;
    }

    /**
     * Rewinds the array (iterator req.)
     *
     * @return void
     */
    public function rewind()
    {
        $this->_position = 0;
    }

    /**
     * Validate data (iterator req.)
     * @return bool
     */
    public function valid()
    {
        return isset($this->_cards[$this->_position]);
    }

    /**
     * Add cart to the collection
     *
     * @param Card_Interface $card
     * @return Card_Collection_Abstract
     */
    protected function _addCard(Card_Interface $card)
    {
        $this->_cards[] = $card;

        return $this;
    }

}
