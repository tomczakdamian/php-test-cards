<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-07
 * @time 13:21
 */

interface Card_Collection_Interface
{
    /**
     * Child class should validate cart type before adding it to
     * the collection
     *
     * @param Card_Interface $card
     * @return Card_Collection_Interface
     */
    public function addCard(Card_Interface $card);
}