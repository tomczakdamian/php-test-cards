<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 12:58
 */

class Card_Collection_Heart
    extends Card_Collection_Abstract
{
    /**
     * Validate cart type and add it to the collection
     *
     * @param Card_Interface $card
     * @return $this|Card_Collection_Interface
     * @throws Exception
     */
    public function addCard(Card_Interface $card)
    {
        if (!$card instanceof Card_Heart) {
            throw new Exception('Invalid cart type');
        }

        $this->_addCard($card);

        return $this;
    }

}