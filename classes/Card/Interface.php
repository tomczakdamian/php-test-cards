<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-07
 * @time 13:18
 */

interface Card_Interface
{
    /**
     * Return the color name in the child class
     *
     * @return string
     */
    public function getColorName();
}