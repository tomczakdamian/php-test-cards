<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 13:25
 */

class Card_Helper
{
    /**
     * Returns color collection with cards
     *
     * @param $colorId
     * @return Cards_Collection_Abstract
     * @throws Exception
     */
    public function getCardCollection($colorId)
    {
        $colorClassName = $this->getColorClassNameById($colorId);

        if ($colorClassName===null) {
            throw new Exception('Invalid color ID');
        }
        
        $collectionClassName = sprintf('Card_Collection_%s', $colorClassName);

        /**
         * @var $collection Cards_Collection_Abstract
         */
        $collection = new $collectionClassName;

        $cards = $this->getColorCards($colorId);

        /**
         * TODO: Allow the collection object to retrieve the array instead of single card objects
         */
        foreach ($cards as $card) {
            $collection->addCard($card);
        }

        return $collection;
    }

    /**
     * Returns array of color cards
     *
     * @param $colorId
     * @return array
     * @throws Exception
     */
    public function getColorCards($colorId)
    {
        $colorClassName = $this->getColorClassNameById($colorId);

        if ($colorClassName===null) {
            throw new Exception('Invalid color ID');
        }

        $cardClassName = sprintf('Card_%s', $colorClassName);

        $cards = array(
            new $cardClassName(Card_Abstract::CARD_VALUE_ACE),
            new $cardClassName(Card_Abstract::CARD_VALUE_TWO),
            new $cardClassName(Card_Abstract::CARD_VALUE_THREE),
            new $cardClassName(Card_Abstract::CARD_VALUE_FOUR),
            new $cardClassName(Card_Abstract::CARD_VALUE_FIVE),
            new $cardClassName(Card_Abstract::CARD_VALUE_SIX),
            new $cardClassName(Card_Abstract::CARD_VALUE_SEVEN),
            new $cardClassName(Card_Abstract::CARD_VALUE_EIGHT),
            new $cardClassName(Card_Abstract::CARD_VALUE_NINE),
            new $cardClassName(Card_Abstract::CARD_VALUE_TEN),
            new $cardClassName(Card_Abstract::CARD_VALUE_JACK),
            new $cardClassName(Card_Abstract::CARD_VALUE_QUEEN),
            new $cardClassName(Card_Abstract::CARD_VALUE_KING),
        );

        return $cards;
    }
    

    /**
     * Returns color class name
     *
     * @param $colorId
     * @return null|string
     */
    public function getColorClassNameById($colorId)
    {
        $colorClassPrefix = null;

        switch ($colorId) {
            case Card_Abstract::CARD_COLOR_CLUB:
                $colorClassPrefix = 'Club';
                break;
            case Card_Abstract::CARD_COLOR_DIAMOND:
                $colorClassPrefix = 'Diamond';
                break;
            case Card_Abstract::CARD_COLOR_HEART:
                $colorClassPrefix = 'Heart';
                break;
            case Card_Abstract::CARD_COLOR_SPADE:
                $colorClassPrefix = 'Spade';
                break;
        }

        return $colorClassPrefix;
    }


}