<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 12:42
 */

abstract class Card_Abstract
    implements Card_Interface
{
    /**
     * Card values
     */
    const CARD_VALUE_ACE = 1;
    const CARD_VALUE_TWO = 2;
    const CARD_VALUE_THREE = 3;
    const CARD_VALUE_FOUR = 4;
    const CARD_VALUE_FIVE = 5;
    const CARD_VALUE_SIX = 6;
    const CARD_VALUE_SEVEN = 7;
    const CARD_VALUE_EIGHT = 8;
    const CARD_VALUE_NINE = 9;
    const CARD_VALUE_TEN = 10;
    const CARD_VALUE_JACK = 11;
    const CARD_VALUE_QUEEN = 12;
    const CARD_VALUE_KING = 13;

    /**
     * Card colors
     */
    const CARD_COLOR_CLUB = 1;
    const CARD_COLOR_DIAMOND = 2;
    const CARD_COLOR_HEART = 3;
    const CARD_COLOR_SPADE = 4;

    /**
     * Card value
     * @var int
     */
    protected $_value = -1;

    /**
     * Card owner holder
     *
     * @var null
     */
    protected $_player = null;

    /**
     * Class Constructor
     *
     * @param $cardValue
     * @throws Exception
     */
    public function __construct($cardValue)
    {
        $this->setValue($cardValue);
    }

    /**
     * Card owner setter
     *
     * @param Player $player
     * @return $this
     */
    public function setPlayer(Player $player = null)
    {
        if ($player===null) {
            $this->_player = null;
        }

        $this->_player = $player;

        return $this;
    }

    /**
     * Card owner getter
     *
     * @return (Player|null)
     */
    public function getPlayer()
    {
        return $this->_player;
    }

    /**
     * Card value setter
     *
     * @param int $type
     * @throws Exception
     * @return $this
     */
    public function setValue($type)
    {
        if (!array_key_exists($type, $this->_getAvailableCardValues())) {
            throw new Exception('Invalid card type');
        }

        $this->_value = $type;

        return $this;
    }

    /**
     * Card value getter
     * @return int
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * Returns card value name
     *
     * @return bool (string|int)
     */
    public function getValueName()
    {
        $value = $this->getValue();

        $types = $this->_getAvailableCardValues();

        if (!array_key_exists($value, $types)) {
            return false;
        }

        return $types[$value];
    }


    /**
     * Returns available card values
     *
     * @return array
     */
    protected function _getAvailableCardValues()
    {
        return array(
            self::CARD_VALUE_ACE => 'Ace',
            self::CARD_VALUE_TWO => self::CARD_VALUE_TWO,
            self::CARD_VALUE_THREE => self::CARD_VALUE_THREE,
            self::CARD_VALUE_FOUR => self::CARD_VALUE_FOUR,
            self::CARD_VALUE_FIVE => self::CARD_VALUE_FIVE,
            self::CARD_VALUE_SIX => self::CARD_VALUE_SIX,
            self::CARD_VALUE_SEVEN => self::CARD_VALUE_SEVEN,
            self::CARD_VALUE_EIGHT => self::CARD_VALUE_EIGHT,
            self::CARD_VALUE_NINE => self::CARD_VALUE_NINE,
            self::CARD_VALUE_TEN => self::CARD_VALUE_TEN,
            self::CARD_VALUE_JACK => 'Jack',
            self::CARD_VALUE_QUEEN => 'Queen',
            self::CARD_VALUE_KING => 'King'
        );
    }
}