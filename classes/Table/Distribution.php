<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 15:58
 */

class Table_Distribution
{
    /**
     * Table object holder
     *
     * @var Table
     */
    protected $_table = null;

    /**
     * All cards holder
     *
     * @var ArrayObject
     */
    protected $_allCards = array();

    /**
     * Class constructor
     *
     * @param Table $table
     */
    public function __construct(Table $table)
    {
        $this->_table = $table;
        $this->_allCards = new ArrayObject();
    }

    /**
     * Returns table object instance
     *
     * @return Table
     */
    public function getTable()
    {
        return $this->_table;
    }

    /**
     * Return all cards from the deck
     *
     * @return ArrayObject
     */
    public function getCardsFromDeck($clear=false)
    {
        if ($this->_allCards->count()>0 && !$clear) {
            return $this->_allCards;
        }

        $allCards = new ArrayObject();

        foreach ($this->getTable()->getDeck()->getColors() as $color) {
            foreach ($color->getCards() as $card) {
                $allCards->append($card);
            }
        }

        $this->_allCards = $allCards;

        return $allCards;
    }

    /**
     * Sort the deck to the perfect sequence ;)
     *
     * @return $this
     */
    public function sortToPerfectSequence()
    {
        $cards = $this->getCardsFromDeck();

        $map = $this->_getCardsMap();

        $sortedCards = array();

        foreach ($map as $cards) {
            $sortedCards = array_merge($sortedCards, $cards);
        }

        $this->_allCards = new ArrayObject($sortedCards);

        return $this;
    }

    /**
     * Shuffle cards
     *
     * @throws Exception
     * @return $this
     */
    public function shuffle()
    {
        $cards = $this->getCardsFromDeck();

        $map = $this->_getCardsMap();

        $shuffle = array();

        $lastAddedValue = null;

        while (count($map)>=1) {
            $ok = false;
            while ($ok===false) {
                $biggestArray = array();
                $keys = array_keys($map);
                shuffle($keys);
                /**
                 * Get the card from the biggest collection.
                 * By this trick the cards disappearing from the array
                 * at a steady pace
                 */
                foreach ($keys as $cardValue) {
                    if ($cardValue!==$lastAddedValue
                        && count($map[$cardValue])>count($biggestArray)) {
                        $biggestArray = $map[$cardValue];
                        $ok = true;
                        $value = $cardValue;
                    }
                }
            }

            shuffle($map[$value]);

            $cardKey = array_rand($map[$value]);

            $shuffle[] = $map[$value][$cardKey];

            unset($map[$value][$cardKey]);

            if (count($map[$value])===0) {
                unset($map[$value]);
            }

            $lastAddedValue = $value;
        }

        $this->_allCards = new ArrayObject($shuffle);

        return $this;
    }

    /**
     * Distribute cards between players
     *
     * @return bool
     * @throws Exception
     */
    public function distributeCardsBetweenPlayers()
    {
        $this->_validate();

        $cards = $this->getCardsFromDeck();

        $players = $this->getTable()->getPlayers();

        $maxCards = $this->getTable()->getMaxNumberOfCardsPerPlayer();

        if (($maxCards*count($players))>count($cards)) {
            throw new Exception('No enough number of cards at the table');
        }

        foreach ($cards as $card) {
            $distributeTheCards = false;

            foreach ($players as $player) {
                if (count($player->getCards())<$maxCards) {
                    $distributeTheCards = true;
                    break;
                }
            }

            if ($distributeTheCards===false) {
                break;
            }

            foreach ($players as $key => $player) {
                if (count($player->getCards())==$maxCards) {
                    continue;
                }

                $player->addCard($card);

                unset($players[$key]);

                $players[] = $player;

                break;
            }
        }

        return true;
    }

    /**
     * Returns debug output content
     *
     * @param null $header
     * @return string
     */
    public function getDebugOutput($header=null)
    {
        $cards = $this->getCardsFromDeck();

        $output = Application::instance()->CLI() ? PHP_EOL : '<br/>';

        if ($header===null) {
            $output .= 'Cards in the deck:';
        } else {
            $output .= $header;
        }

        $output .= Application::instance()->CLI() ? PHP_EOL : '<br/>';

        $i = 1;

        foreach ($cards as $card) {
            $output .= sprintf('%s. %s - %s',$i, $card->getColorName(), $card->getValueName());
            $output .= Application::instance()->CLI() ? PHP_EOL : '<br/>';
            $i++;
        }

        return $output;
    }


    /**
     * Validate table
     *
     * @return bool
     * @throws Exception
     */
    protected function _validate()
    {
        if (!$this->getTable()->getDeck() instanceof Deck) {
            throw new Exception('Deck is not on the table');
        }

        if (count($this->getTable()->getPlayers())===0) {
            throw new Exception('No players at the table');
        }

        return true;
    }

    /**
     * Returns cards map array
     * (value->cards)
     *
     * @return array
     */
    protected function _getCardsMap()
    {
        $cards = $this->getCardsFromDeck();

        $map = array();

        foreach ($cards as $card) {
            if (!isset($map[$card->getValue()])) {
                $map[$card->getValue()] = array();
            }
            $map[$card->getValue()][] = $card;
        }

        return $map;
    }


}
