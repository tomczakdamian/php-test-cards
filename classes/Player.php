<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 14:53
 */

class Player
{
    /**
     * Firstname holder
     *
     * @var string
     */
    protected $_firstname = null;

    /**
     * Cards holder
     *
     * @var array
     */
    protected $_cards = array();

    /**
     * Class constructor
     *
     * @param $firstname
     */
    public function __construct($firstname)
    {
        $this->_firstname = $firstname;
    }

    /**
     * Firstname getter
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->_firstname;
    }

    /**
     * Add card to the player
     *
     * @param Card_Interface $card
     * @return $this
     */
    public function addCard(Card_Interface $card)
    {
        $this->_cards[] = $card;

        $card->setPlayer($this);

        return $this;
    }

    /**
     * Return player cards array
     *
     * @return array
     */
    public function getCards()
    {
        return $this->_cards;
    }


}