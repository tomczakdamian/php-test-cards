<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 14:53
 */

class Table
{
    /**
     * Deck holder
     *
     * @var (Deck|null)
     */
    protected $_deck = null;

    /**
     * @var Table_Distribution
     */
    protected $_distributionObject = null;

    /**
     * Maximum number of cards per player holder
     *
     * @var int
     */
    protected $_numberOfCardsPerPlayer = 4;

    /**
     * Players holder
     *
     * @var array
     */
    protected $_players = array();

    /**
     * Class constructor
     *
     * @param Deck $deck
     */
    public function __construct(Deck $deck = null)
    {
        if ($deck instanceof Deck) {
            $this->setDeck($deck);
        }
    }

    /**
     * Number of cards per player setter
     *
     * @param $value
     * @return $this
     */
    public function setMaxNumberOfCardsPerPlayer($value)
    {
        $this->_numberOfCardsPerPlayer = (int) $value;

        return $this;
    }

    /**
     * Maximum number of cards per player getter
     *
     * @return int
     */
    public function getMaxNumberOfCardsPerPlayer()
    {
        return $this->_numberOfCardsPerPlayer;
    }

    /**
     * Deck getter
     *
     * @return (Deck|null)
     */
    public function getDeck()
    {
        return $this->_deck;
    }

    /**
     * Deck setter
     *
     * @param Deck $deck
     * @return $this
     */
    public function setDeck(Deck $deck)
    {
        $this->_deck = $deck;

        return $this;
    }

    /**
     * Players getter
     *
     * @return array
     */
    public function getPlayers()
    {
        return $this->_players;
    }

    /**
     * Add player to the table
     *
     * @param Player $player
     * @return $this
     */
    public function addPlayer(Player $player)
    {
        $this->_players[] = $player;

        return $this;
    }

    /**
     * Shuffle cards
     *
     * @return bool
     */
    public function shuffleDeck()
    {
        if (!$this->getDeck() instanceof Deck) {
            throw new Exception('Deck is not on the table');
        }

        return $this->getDeck()->shuffle();
    }

    /**
     * Returns distribution object
     *
     * @return Table_Distribution
     */
    public function getDistributionObject()
    {
        /**
         * Lazy loading
         */
        if ($this->_distributionObject===null) {
            $this->_distributionObject = new Table_Distribution($this);
        }

        $distribution = $this->_distributionObject;

        return $distribution;
    }



}