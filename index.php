<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 12:32
 */

require_once 'autoloader.php';

if (! (bool) Application::instance()->getConfigValue('settings/enabled')) {
    die('Application is disabled');
}

if (Application::instance()->getConfigValue('settings/display_errors')) {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
}

$helper = new Card_Helper;

$deck = new Deck(array(
    $helper->getCardCollection(Card_Abstract::CARD_COLOR_CLUB),
    $helper->getCardCollection(Card_Abstract::CARD_COLOR_DIAMOND),
    $helper->getCardCollection(Card_Abstract::CARD_COLOR_HEART),
    $helper->getCardCollection(Card_Abstract::CARD_COLOR_SPADE)
));

$table = new Table($deck);

/**
 * Set the number of the cards per player
 * and invite players to the table
 */
$table->setMaxNumberOfCardsPerPlayer(7)
    ->addPlayer(new Player('Damian'))
    ->addPlayer(new Player('Sonia'))
    ->addPlayer(new Player('Slawek'))
    ->addPlayer(new Player('Luca'));

/**
 * Sort the deck to the perfect sequence
 */
$table->getDistributionObject()->sortToPerfectSequence();

if (Application::instance()->getConfigValue('settings/debug')) {
    echo $table->getDistributionObject()->getDebugOutput('Perfect Sequence:');
}

/**
 * Shuffle the deck
 */
$table->getDistributionObject()->shuffle();

if (Application::instance()->getConfigValue('settings/debug')) {
    echo $table->getDistributionObject()->getDebugOutput('Shuffled Cards:');
}

/**
 * Distribute the deck between the players
 */
$table->getDistributionObject()->distributeCardsBetweenPlayers();

if (Application::instance()->CLI()) {
    include 'view/cli.phtml';
} else {
    include 'view/html.phtml';
}