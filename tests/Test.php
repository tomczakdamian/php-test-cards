<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-07
 * @time 11:55
 */

class Test extends PHPUnit_Framework_TestCase
{
    public function testNumberOfColorAndCards()
    {
        $helper = new Card_Helper;

        $deck = new Deck(array(
            $helper->getCardCollection(Card_Abstract::CARD_COLOR_CLUB),
            $helper->getCardCollection(Card_Abstract::CARD_COLOR_DIAMOND),
            $helper->getCardCollection(Card_Abstract::CARD_COLOR_HEART),
            $helper->getCardCollection(Card_Abstract::CARD_COLOR_SPADE)
        ));

        $this->assertEquals(4, count($deck->getColors()));

        $numberOfCards = 0;

        foreach ($deck->getColors() as $color) {
            $numberOfCards += count($color->getCards());
        }

        $this->assertEquals(52, $numberOfCards);

        return $deck;
    }

    /**
     * @depends testNumberOfColorAndCards
     */
    public function testNumberOfPlayers(Deck $deck)
    {
        $table = new Table($deck);

        $table->setMaxNumberOfCardsPerPlayer(7)
            ->addPlayer(new Player('Damian'))
            ->addPlayer(new Player('Sonia'))
            ->addPlayer(new Player('Slawek'))
            ->addPlayer(new Player('Luca'));

        $numberOfPlayers = count($table->getPlayers());

        $this->assertEquals(4, $numberOfPlayers);

        return $table;
    }

    /**
     * @depends testNumberOfPlayers
     */
    public function testNumberOfCardsPerPlayer(Table $table)
    {
        $this->assertEquals(7, $table->getMaxNumberOfCardsPerPlayer());

        return $table;
    }

    /**
     * @depends testNumberOfCardsPerPlayer
     */
    public function testPerfectSequence(Table $table)
    {
        $distributionObject = $table->getDistributionObject()
            ->sortToPerfectSequence();

        $i = 0;
        $x = 1;

        foreach ($distributionObject->getCardsFromDeck() as $card) {
            $validCardValue = $x;

            $this->assertEquals($card->getValue(), $x);

            $i++;

            if ($i==count($table->getDeck()->getColors())) {
                $i = 0;
                $x++;
            }
        }

        return $distributionObject;
    }

    /**
     * @depends testPerfectSequence
     */
    public function testShuffleSequence(Table_Distribution $distributionObject)
    {
        $distributionObject->shuffle();

        $lastValue = null;

        foreach ($distributionObject->getCardsFromDeck() as $card) {
            $this->assertNotEquals($card->getValue(), $lastValue);
            $lastValue = $card->getValue();
        }

        return $distributionObject;
    }

    /**
     * @depends testShuffleSequence
     */
    public function testPlayers(Table_Distribution $distributionObject)
    {
        $distributionObject->distributeCardsBetweenPlayers();

        $table = $distributionObject->getTable();

        foreach ($table->getPlayers() as $player) {
            $this->assertEquals($table->getMaxNumberOfCardsPerPlayer(), count($player->getCards()));
        }

        return true;
    }

}
 