<?php
/**
 * @author Tomczak Damian <tomczak.damian@outlook.com>
 * @date 2015-02-06
 * @time 20:44
 */

define('DS', '/');

spl_autoload_register(function ($className) {
    $parts = explode('_', $className);

    if (!empty($parts)) {
        foreach ($parts as $key => $value) {
            $parts[$key] = ucfirst($value);
        }

        $filePath = 'classes' . DS . implode(DS, $parts) . '.php';

        if (file_exists($filePath)) {
            require_once $filePath;
        }
    }
});